/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useCallback, useState } from 'react'
import {
  NativeSyntheticEvent,
  StatusBar,
  TextInputSubmitEditingEventData,
} from 'react-native'
import { ReactMemo, strings, setMCIP } from '~/common'
import { Form } from '~/components'
import { useNavigation, StackActions } from '@react-navigation/native'
import { DEFAULT_IP } from '~/config'

const SettingPage = () => {
  const navigation = useNavigation()
  const [mcIP, setValueMCIP] = useState<string>(DEFAULT_IP)

  const onDoneMediaCenter = useCallback(
    (_: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => {
      setMCIP(mcIP).then(() => {
        navigation.dispatch(
          StackActions.replace('PositionSetting', { ip: mcIP }),
        )
      })
    },
    [mcIP],
  )
  const onChangeTextIP = useCallback((targetValue: string) => {
    setValueMCIP(targetValue)
  }, [])

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Form
        title={strings.MediaCenterIP.title}
        subTitle={strings.MediaCenterIP.subTitle}
        placeholder={strings.MediaCenterIP.placeholder}
        returnKeyType="done"
        multiline={false}
        value={mcIP}
        onSubmitEditing={onDoneMediaCenter}
        onChangeText={onChangeTextIP}
        autoFocus
      />
    </>
  )
}

export default ReactMemo(SettingPage)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import { StackActions, useNavigation } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import axios from 'axios'
import React, { useCallback, useState } from 'react'
import {
  NativeSyntheticEvent,
  StatusBar,
  TextInputSubmitEditingEventData,
} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import {
  download,
  MediaItem,
  ReactMemo,
  removeVideoFiles,
  strings,
} from '~/common'
import { Form } from '~/components'
import { PORT_HTTP, DEFAULT_POSITION } from '~/config'
import { RootStackParamList } from './page.types'

type Props = StackScreenProps<RootStackParamList, 'PositionSetting'>

const PositionPage = (props: Props) => {
  const {
    route: {
      params: { ip },
    },
  } = props

  // props
  const [isDownload, setDownload] = useState(false)
  const navigation = useNavigation()
  const [message, setMessage] = useState('')
  const [position, setPosition] = useState<string>(DEFAULT_POSITION)

  const onDonePosition = useCallback(
    async (_: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => {
      setDownload(true)
      const rootURL = `http://${ip}:${PORT_HTTP}`
      const apiURL = `${rootURL}/media-list/?position=${position}`
      try {
        const { data } = await axios.request<Array<MediaItem>>({
          url: apiURL,
          method: 'GET',
        })
        await removeVideoFiles()

        const fileLength = data.length
        for (let i = 0; i < fileLength; i++) {
          const mediaItem = data[i]
          setMessage(`File download...(${i + 1}/${fileLength})`)
          try {
            await download(
              `${rootURL}/${mediaItem.uri.replace(/^\//, '')}`,
              mediaItem.fileName,
            )
          } catch (e) {}
        }
        setDownload(false)
        navigation.dispatch(StackActions.replace('Player'))
      } catch (e) {}
    },
    [ip, position],
  )

  const onChangeTextIP = useCallback(
    (targetValue: string) => setPosition(targetValue),
    [],
  )

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Form
        title={strings.Position.title}
        subTitle={strings.Position.subTitle}
        placeholder={strings.Position.placeholder}
        returnKeyType="done"
        keyboardType="number-pad"
        multiline={false}
        value={position}
        onSubmitEditing={onDonePosition}
        onChangeText={onChangeTextIP}
        autoFocus
      />
      <Spinner visible={isDownload} textContent={message} />
    </>
  )
}

export default ReactMemo(PositionPage)

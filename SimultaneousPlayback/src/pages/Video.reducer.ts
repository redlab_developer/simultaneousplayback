import { useReducer } from 'react'

export type ActionByVideo =
  | { type: 'init' }
  | {
      type: 'loadStart'
      payload: string
    }
  | { type: 'loadSuccess' }
  | { type: 'play' }

export type StateByVideo = {
  file?: string
  isLoad: boolean
  isPaused: boolean
  state: ActionByVideo['type']
}

export const initStateByVideo: StateByVideo = {
  isLoad: false,
  isPaused: false,
  state: 'init',
}

export const reducerByVideo = (
  state: StateByVideo = initStateByVideo,
  action: ActionByVideo,
): StateByVideo => {
  if (action.type === 'init') {
    return {
      file: undefined,
      isLoad: false,
      isPaused: false,
      state: action.type,
    }
  } else if (action.type === 'loadStart') {
    return {
      file: action.payload,
      isLoad: false,
      isPaused: false,
      state: action.type,
    }
  } else if (action.type === 'loadSuccess') {
    return {
      isPaused: true,
      isLoad: true,
      state: action.type,
      file: state.file,
    }
  } else if (action.type === 'play') {
    return {
      ...state,
      state: action.type,
      isPaused: false,
    }
  }
  return state
}

export const useReducerByVideo = () =>
  useReducer(reducerByVideo, initStateByVideo)

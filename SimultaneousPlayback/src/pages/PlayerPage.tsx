import { StackActions, useNavigation } from '@react-navigation/native'
import { useDidMount, useWillUnmount } from 'beautiful-react-hooks'
import React, { useCallback, useEffect, useRef, useState, useMemo } from 'react'
import { Alert, AppState, AppStateStatus, ToastAndroid } from 'react-native'
import RNFS from 'react-native-fs'
import KeyEvent, { KeyEventProps } from 'react-native-keyevent'
import Video, { VideoProperties } from 'react-native-video'
import styled from 'styled-components/native'
import { getMCIP, ReactMemo, resetIp, strings, WSNtp } from '~/common'
import { useReducerByVideo } from './Video.reducer'
import { DIR_DOWNLOAD } from '~/config'

const RootContainer = styled.View`
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: #000;
`

const PlayerPage = () => {
  const refIPResetCount = useRef(0)
  const navigation = useNavigation()
  const refVideo = useRef<Video>(null)
  const refWS = useRef<WSNtp>()

  const [{ file, isPaused, state }, dispatch] = useReducerByVideo()

  const [mcIP, setIP] = useState<string | undefined>(undefined)

  /**
   * Weboscket disconnection
   */
  const disconnectionWebsocket = useCallback(() => {
    if (refWS.current) {
      refWS.current.close()
      refWS.current = undefined
    }
  }, [])

  useEffect(() => {
    if (state === 'loadSuccess') {
      if (refWS.current) {
        refWS.current.send('ready')
      }
    }
  }, [state])

  /**
   * Websocket connection
   */
  const connectionWebsocket = useCallback((ip: string) => {
    const wsURL = `ws://${ip}:8080/ws`
    const onError = () => {
      Alert.alert(
        strings.PlayerByDisonnect.title,
        undefined,
        [
          {
            text: strings.PlayerByDisonnect.cancel,
            onPress: () =>
              resetIp().then(() => {
                navigation.dispatch(StackActions.replace('Setting'))
              }),
            style: 'cancel',
          },
          {
            text: strings.PlayerByDisonnect.ok,
            onPress: () => connectionWebsocket(ip),
          },
        ],
        { cancelable: false },
      )
    }

    const mWebsocketNTP = new WSNtp(
      wsURL,
      (message) => {
        if (message.indexOf('P') === 0) {
          const playTime = parseInt(message.replace(/^P\:\s/, ''), 10)
          const latestTime = Math.min(
            Math.max(playTime - mWebsocketNTP.time(), 0),
            2000,
          )
          if (refVideo.current) {
            refVideo.current.seek(0)
          }

          setTimeout(() => {
            dispatch({ type: 'play' })
            setTimeout(() => {
              // 다시 ntp 싱크
              mWebsocketNTP.resync()
            }, 1000)
          }, latestTime)
        } else {
          const fullPath = `file://${DIR_DOWNLOAD}/${message.replace(
            /^R\:\s/,
            '',
          )}`
          dispatch({ type: 'loadStart', payload: fullPath })
        }
      },
      onError,
    )
    refWS.current = mWebsocketNTP
  }, [])

  const onChangeState = useCallback(
    (state: AppStateStatus) => {
      if (state === 'background') {
        disconnectionWebsocket()
        dispatch({ type: 'init' })
      } else if (state === 'active') {
        if (mcIP) {
          connectionWebsocket(mcIP)
        }
      }
    },
    [mcIP, dispatch],
  )

  useDidMount(() => {
    getMCIP()
      .then((value) => {
        if (value === undefined) {
          navigation.dispatch(StackActions.replace('Setting'))
        } else {
          setIP(value)
        }
      })
      .catch(() => {
        resetIp().then(() => {
          navigation.dispatch(StackActions.replace('Setting'))
        })
      })

    AppState.addEventListener('change', onChangeState)

    KeyEvent.onKeyUpListener((keyEvent: KeyEventProps) => {
      if (keyEvent.keyCode === 19) {
        refIPResetCount.current = refIPResetCount.current + 1
        if (refIPResetCount.current >= 3) {
          ToastAndroid.show('Reset IP', ToastAndroid.SHORT)
        }
        if (refIPResetCount.current === 5) {
          refIPResetCount.current = 0
          resetIp().then(() => {
            navigation.dispatch(StackActions.replace('Setting'))
          })
        }
      } else {
        refIPResetCount.current = 0
      }
    })
  })

  useWillUnmount(() => {
    KeyEvent.removeKeyUpListener()
    disconnectionWebsocket()
    refIPResetCount.current = 0
  })

  useEffect(() => {
    // ip 주소가 변경될 경우 처리
    disconnectionWebsocket()
    if (mcIP !== undefined) {
      connectionWebsocket(mcIP)
    }
    return disconnectionWebsocket
  }, [mcIP, connectionWebsocket, disconnectionWebsocket, onChangeState])

  const onLoad = useCallback(() => dispatch({ type: 'loadSuccess' }), [
    dispatch,
  ])

  return useMemo(
    () => (
      <RootContainer>
        {file && (
          <Video
            style={styleByVideo}
            source={{ uri: file }}
            onLoad={onLoad}
            resizeMode="stretch"
            paused={isPaused}
            ref={refVideo}
          />
        )}
      </RootContainer>
    ),
    [file, isPaused, onLoad],
  )
}

const styleByVideo: VideoProperties['style'] = {
  position: 'absolute',
  left: 0,
  top: 0,
  right: 0,
  bottom: 0,
}

export default ReactMemo(PlayerPage)

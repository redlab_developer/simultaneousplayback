import RNFS from 'react-native-fs'

/**
 * Base 화면 높이
 */
export const BASE_HEIGHT = 720

/**
 * WebSocket PORT
 */
export const PORT_WS = 8080

/**
 * 서버 통신 PORT
 */
export const PORT_HTTP = 3000

/**
 * Download Directory
 */
export const DIR_DOWNLOAD = RNFS.DocumentDirectoryPath

/**
 * Settingpage 에 들어가는 default ip 주소, 레노버 노트북에 hotspot ip 주소
 */
export const DEFAULT_IP = '10.42.0.1'
// export const DEFAULT_IP = '172.16.20.110'

/**
 * PostionPage 에 들어가는 default postion,
 * server 에 media dir 안에 폴더명
 */
export const DEFAULT_POSITION = '1'

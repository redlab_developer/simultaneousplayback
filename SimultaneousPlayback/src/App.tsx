import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { useDidMount } from 'beautiful-react-hooks'
import React, { useState } from 'react'
import { StatusBar } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import PlayerPage from '~/pages/PlayerPage'
import SettingPage from '~/pages/SettingPage'
import PositionPage from '~/pages/PositionPage'
import { getMCIP } from './common'

const Stack = createStackNavigator()

type MCIP = undefined | null | string

const App = () => {
  const [isLoad, setLoad] = useState<MCIP>(undefined)

  useDidMount(() => {
    getMCIP().then((value) => {
      if (value) {
        setLoad(value)
      } else {
        setLoad(null)
      }
    })
  })
  // isLoad === null ? 'Setting' : 'Player'
  return (
    <>
      <StatusBar barStyle="dark-content" />
      {isLoad !== undefined && (
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{ headerShown: false }}
            initialRouteName={isLoad === null ? 'Setting' : 'Player'}>
            <Stack.Screen name="Setting" component={SettingPage} />
            <Stack.Screen name="PositionSetting" component={PositionPage} />
            <Stack.Screen name="Player" component={PlayerPage} />
          </Stack.Navigator>
        </NavigationContainer>
      )}
      <Spinner visible={isLoad === undefined} textContent="loading..." />
    </>
  )
}

export default App

import React from 'react'
import Video, { VideoProperties } from 'react-native-video'
import styled from 'styled-components/native'
import { ReactMemo } from '~/common'

const StyleVideo = styled(Video)`
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
`

export const FullScreenVideo = ReactMemo((props: VideoProperties) => (
  <StyleVideo {...props} resizeMode="stretch" fullscreen={true} />
))

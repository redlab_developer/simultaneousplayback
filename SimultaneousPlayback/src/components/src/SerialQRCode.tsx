import React from 'react'
import QRCode from 'react-native-qrcode-svg'
import styled from 'styled-components/native'
import { ReactMemo } from '~/common'

type SerialQRCodeProps = {
  size?: number
}

const Container = styled.View<SerialQRCodeProps>`
  width: ${({ size = 200 }) => size + 20}px;
  height: ${({ size = 200 }) => size + 20}px;
  justify-content: center;
  align-items: center;
  background-color: white;
`

export const SerialQRCode = ReactMemo((props: SerialQRCodeProps) => {
  const { size = 200 } = props
  return (
    <Container size={size}>
      <QRCode value="http://redlab.co.kr" size={size} />
    </Container>
  )
})

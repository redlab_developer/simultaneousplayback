import React from 'react'
import { TextInput, TextInputProps } from 'react-native'
import styled from 'styled-components/native'
import { toPx, ReactMemo } from '~/common'

const Container = styled.View`
  width: ${toPx(309)};
  height: ${toPx(64)};
  background-color: #ffffff;
  justify-content: center;
  align-items: center;
  border-radius: ${toPx(2)};
`
const SText = styled.TextInput`
  color: #444444;
  position: absolute;
  top: ${toPx(12)};
  bottom: ${toPx(12)};
  left: ${toPx(21)};
  right: ${toPx(21)};
  padding: 0;
  border-bottom-width: 1px;
  border-color: #444444;
`

export const InputText = ReactMemo((props: TextInputProps) => {
  const {placeholderTextColor = '#444444', ...rest} = props
  return (
    <Container>
      <SText {...rest} placeholderTextColor={placeholderTextColor} />
    </Container>
  )
})

import React from 'react'
import { ProgressBarAndroid } from 'react-native'
import styled from 'styled-components/native'
import { ReactMemo, toPx } from '~/common'

const RootContainer = styled.SafeAreaView`
  flex: 1;
  flex-direction: row;
  background-color: #1f292e;
  justify-content: center;
  align-items: center;
  padding-left: ${toPx(151)};
  padding-right: ${toPx(151)};
`

export const FullLoading = ReactMemo(() => (
  <RootContainer>
    <ProgressBarAndroid />
  </RootContainer>
))

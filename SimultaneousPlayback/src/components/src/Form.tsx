import React, { useMemo } from 'react'
import { TextInputProps } from 'react-native'
import styled from 'styled-components/native'
import { ReactMemo, toPx } from '~/common'
import { InputText } from './InputText'

const RootContainer = styled.SafeAreaView`
  flex: 1;
  flex-direction: row;
  background-color: #1f292e;
  justify-content: center;
  align-items: center;
  padding-left: ${toPx(151)};
  padding-right: ${toPx(151)};
`

const InfoContainer = styled.View`
  flex: 1;
`

const Title = styled.Text`
  font-size: ${toPx(36)};
  color: #fff;
`

const SubTitle = styled.Text`
  font-size: ${toPx(18)};
  color: #fff;
  opacity: 0.5;
  margin-top: ${toPx(18)};
  line-height: ${toPx(30)};
`

type FormProps = {
  title: string
  subTitle: string
} & TextInputProps

export const Form = ReactMemo((props: FormProps) => {
  const {
    title,
    subTitle,
    returnKeyType = 'done',
    autoFocus = true,
    multiline = false,
    ...rest
  } = props

  const DrawTitle = useMemo(
    () => (
      <InfoContainer>
        <Title>{title}</Title>
        <SubTitle>{subTitle}</SubTitle>
      </InfoContainer>
    ),
    [title, subTitle],
  )
  return (
    <RootContainer>
      {DrawTitle}
      <InputText
        {...rest}
        multiline={multiline}
        returnKeyType={returnKeyType}
        autoFocus
      />
    </RootContainer>
  )
})

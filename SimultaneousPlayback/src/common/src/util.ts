import RNFS, { DownloadResult } from 'react-native-fs'
import { DIR_DOWNLOAD } from '~/config'

/**
 * 파일 다운로드
 * @param fromUrl
 * @param toFile
 */
export function download(fromUrl: string, toFile: string) {
  return new Promise<DownloadResult | undefined>((resolve) => {
    RNFS.downloadFile({
      fromUrl,
      toFile: `${DIR_DOWNLOAD}/${toFile}`,
    })
      .promise.then((data) => {
        resolve(data)
      })
      .catch(() => {
        resolve(undefined)
      })
  })
}

/**
 * 저장소에 video 파일을 모두 지운다.
 * @param path
 */
export async function removeVideoFiles(path: string = DIR_DOWNLOAD) {
  const readDir = await RNFS.readDir(path)
  if (readDir.length >= 1) {
    for (let fileItem of readDir) {
      if (fileItem.isFile()) {
        const isVideo = /.(3gp|mp4|m4p|m4v|mpg|mpeg|mp2|mpe|mpv|webm|mkv|flv|vob|ogv|ogg|avi|mov|qt|ts|mts|m2ts|wmv)$/gis.test(
          fileItem.path,
        )
        if (isVideo) {
          await RNFS.unlink(fileItem.path)
        }
      }
    }
  }
}

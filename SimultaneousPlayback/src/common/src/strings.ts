const MediaCenterIP = {
  title: 'Please enter Manager IP',
  subTitle: `Please enter IP address.\nExample:192.168.0.1`,
  placeholder: 'Manager Ip...',
}

const Position = {
  title: 'Please enter Device Position',
  subTitle: `Please enter Device Postion.(1-4)\nExample:1`,
  placeholder: 'number(1-4)...',
}

const PlayerByDisonnect = {
  title: 'Disconnect Manager',
  ok: 'reconnect',
  cancel: 'go setting',
}

export const strings = { MediaCenterIP, Position, PlayerByDisonnect }

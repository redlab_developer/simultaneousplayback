import React, {
  ComponentType,
  ComponentProps,
  MemoExoticComponent,
} from 'react'
import isEqual from 'react-fast-compare'

export function ReactMemo<T extends ComponentType<any>>(
  Component: T,
  propsAreEqual: (
    prevProps: Readonly<ComponentProps<T>>,
    nextProps: Readonly<ComponentProps<T>>,
  ) => boolean = isEqual,
): MemoExoticComponent<T> {
  return React.memo(Component, propsAreEqual)
}

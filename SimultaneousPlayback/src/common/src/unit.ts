import { Dimensions } from 'react-native'
import { BASE_HEIGHT } from '~/config'

const calcValue = Math.floor(Dimensions.get('window').height) / BASE_HEIGHT

/**
 *
 * @param num
 */
export const toPx = (num: number | string) => {
  if (typeof num === 'string') {
    const value = parseInt(num, 10)
    if (isNaN(value)) {
      return 0
    }
    return `${value * calcValue}px`
  }

  return `${num * calcValue}px`
}

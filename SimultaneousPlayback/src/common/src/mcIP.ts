import asyncStorage from '@react-native-community/async-storage'

const KEY = 'MC_IP'

let loadIp: string | undefined = undefined

export const getMCIP = async (): Promise<string | undefined> => {
  if (loadIp !== undefined) {
    return loadIp
  }
  try {
    const value = await asyncStorage.getItem(KEY)
    if (value === null) {
      return undefined
    }
    loadIp = value
    return value
  } catch (e) {
    return undefined
  }
}

export const setMCIP = (ip: string) => {
  loadIp = ip
  return asyncStorage.setItem(KEY, ip)
}

export const resetIp = async () => {
  loadIp = undefined
  try {
    await asyncStorage.removeItem(KEY)
    console.log(await getMCIP())
  } catch (e) {
    console.log(e)
  }
}

export type MediaItem = {
  fileName: string
  duration: 60000
  uri: string
}

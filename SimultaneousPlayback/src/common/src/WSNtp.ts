import autobind from 'autobind-decorator'
// import autobind from 'auto-bind'

type WSNtpMessage = {
  type: 'time:'
  ctt: number
  stt: number
}

type NTPItem = {
  ctt: number
  stt: number
  client_receive_time: number
  round_trip_time: number
  offset: number
}

export class WSNtp {
  /**
   * @type {number}
   * @private
   */
  private syncInterval = 300
  /**
   * @type {number}
   * @private
   */
  private syncCount = 5

  /**
   * @type {Array<number>}
   * @private
   */
  private syncArray = new Array<NTPItem>()

  /**
   * @type {number}
   * @private
   */
  private syncCorrection = 0

  /**
   * @type {number}
   * @private
   */
  private syncRoundTrip = Infinity

  /**
   * @type {(number|undefined})
   * @private
   */
  private intervalHandle?: number

  /**
   * @type {(WebSocket|undefined)}
   * @private
   */
  private mWs: WebSocket

  private isInitNTPSyncSuccess = false

  private cbNTPReady?: Function

  constructor(
    url: string,
    private onMessage: (message: string) => void,
    private onError: () => void,
  ) {
    // autobind(this)
    this.mWs = new WebSocket(url)
    this.initWebSocket()
  }

  @autobind
  private initWebSocket() {
    this.mWs.onopen = () => {
      this.resync()
    }

    this.mWs.onerror = this.onError
    this.mWs.onmessage = (event: WebSocketMessageEvent) => {
      // if(event.data)
      const message = String(event.data)

      if (message.indexOf('P') === 0 || message.indexOf('R') === 0) {
        this.onMessage(event.data)
        return
      }
      try {
        const response = JSON.parse(message) as WSNtpMessage
        if (response.type === 'time:') {
          const client_receive_time: number = Date.now()
          const round_trip_time = client_receive_time - response.ctt
          const offset = response.stt - (response.ctt + round_trip_time / 2)
          const syncItem: NTPItem = {
            client_receive_time,
            round_trip_time: client_receive_time - response.ctt,
            offset,
            ctt: response.ctt,
            stt: response.stt,
          }
          this.syncArray.push(syncItem)
          if (this.syncArray.length >= this.syncCount) {
            this.newTimeSync()
          }
        } else {
          this.onMessage(event.data)
        }
      } catch (e) {
        this.onMessage(event.data)
        // this.onMessage(event.data)
      }
    }
  }

  @autobind
  public close() {
    if (this.intervalHandle) {
      clearInterval(this.intervalHandle)
      this.intervalHandle = undefined
    }
    this.mWs.close()
  }

  @autobind
  public send(msg: string) {
    if (this.mWs) {
      this.mWs.send(msg)
    }
  }

  @autobind
  private newTimeSync() {
    let offsetTotal = 0
    let rtTotal = 0
    let i = 0
    for (i = 0; i < this.syncArray.length; i++) {
      if (this.syncArray[i]) {
        offsetTotal += this.syncArray[i].offset
        rtTotal += this.syncArray[i].round_trip_time
      } else {
        break
      }
    }
    this.syncCorrection = offsetTotal / i
    this.syncRoundTrip = rtTotal / i
  }

  @autobind
  public time() {
    return Date.now() + this.syncCorrection
  }

  @autobind
  public offset() {
    return this.syncCorrection
  }

  @autobind
  public roundTrip() {
    return this.syncRoundTrip
  }

  /**
   * @public
   */
  @autobind
  public resync() {
    this.syncArray = []
    if (this.intervalHandle) {
      clearInterval(this.intervalHandle)
    }
    this.intervalHandle = setInterval(() => {
      if (this.syncArray.length < this.syncCount) {
        if (this.mWs.send) {
          this.mWs.send(`get_time:${Date.now()}`)
        }
      } else {
        if (this.intervalHandle) {
          clearInterval(this.intervalHandle)
          if (this.isInitNTPSyncSuccess === false) {
            this.isInitNTPSyncSuccess = true
          }
          if (this.cbNTPReady) {
            this.cbNTPReady()
            this.cbNTPReady = undefined
          }
        }
      }
    }, this.syncInterval)
  }

  @autobind
  public onceReadyNTP(cb: Function) {
    this.cbNTPReady = undefined
    if (this.isInitNTPSyncSuccess) {
      cb()
      return
    }
    this.cbNTPReady = cb
  }
}

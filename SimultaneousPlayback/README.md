# SP-Player

## Configs

### 글자

`src/common/src/strings.ts` 에 화면에서 사용되고 있는 문자열들 관리

### 기본 설정

- BASE_HEIGHT : 디자인 작업 이후 실제 화면에 적용 시, 화면 크기 조절용
- PORT_WS : sp-manager 에 ws port 번호
- PORT_HTTP : sp-manager 에 http port 번호
- DIR_DOWNLOAD : 현재 App data dir 에 추가함
- DEFAULT_IP : 화면에 자동으로 입려된 IP 주소
- DEFAULT_POSITION : Device 에 position

## Pages

- PlayerPage : 재생 화면
  - WebSocket+NTP, Video를 이용해서 영상을 재생한다.
  - 위 5번 누를 경우, Setting Page로 이동
- SettingPage : sp-manager 에 ip 를 설정한다.
- PostionPage : Device에 position 을 설정한다. SerringPage를 통해서만 갈 수 있어야한다.



# SyncPlayer

* 작성일 : 2020-06-30
* 작성자 : REDLAB 김동현 부장
* e-mail : kimdonghyun@redlab.co.kr





# Server(notebook)

* Hotspot 켜기
* Manager 실행 방법
  * Manager 콘텐츠 변경 방법



## Hotspot 켜기

1. 화면 우측 상단 메뉴 중 `Wi-Fi Settings` 클릭
![11](./documents/images/11.jpeg)




2. Settings 창 상단에 메뉴버튼 클릭 후 `Turn On Wifi Hopspot` 클릭
![12](./documents/images/12.jpeg)


3. `Turn On` 클릭 
![13](./documents/images/13.jpeg)








4. 결과 안내
![14](./documents/images/14.jpeg)
>Device가 Wi-Fi 에 연결되어 있어야 합니다.
* Natwork Name : ThinkPad-E125
* Password: 비밀번호는 변경될 수 있습니다.





































## **Manager 실행**
Password: ••••••••

1.  Terminal.app 실행

![08](./documents/images/08.jpeg)

















2. `cd SyncServer/` 입력 후 `Enter`

   > cd Sy 까지 입력하고 키보드에 Tab키를 누르면 cd SyncServer/ 가 자동완성됩니다.

![09](./documents/images/09.jpeg)

3. `npm start` 입력후 `Enter`

![10](./documents/images/10.jpeg)





### Server 기타 기능

영상소스 변경

1. 노트북의 `/home/foncast/SyncServer/media` 경로의 디렉토리로 이동
2. `1, 2, 3, 4` 번호의 폴더에 영상을 `00.mp4`~`03.mp4` 파일명 중 변경할 파일명으로 각각 번호의 폴더에 영상 저장 (폴더의 번호는 셋톱박스의 번호와 동일)
3. 셋톱박스 설정 다시 진행



















# Device

* Networek Setting(Device)
* Application 실행
  * Network Disconnection(Setting)
  * Setting Page



## Network Setting(Device)

1. Setting 이동

![01](./documents/images/01.png)





















2. Network 

![02](./documents/images/02.png)







3. TinkPad-E125 연결

![03](./documents/images/03.png)









## Application 실행

1. Application 실행

![04](./documents/images/04.png)





































### Network Disconnection(Setting)

![05](./documents/images/05.png)



* GO SETTING : Manager Server(notebook) 과 연결을 다시하거나, 새로운 영상을 다시 받는다.
* **RECONNECT : 다시 접속 시도한다.**

> Player 화면에서 리모컨 위 버튼을 5번 눌러도 Setting Page로 이동된다.

























### Setting Page

Manager 에 IP를 다시 설정하거나, **영상을 다시 다운로드 할때 사용된다.**

![06](./documents/images/06.png)

Manager IP 를 입력하고 `Done` 을 누른다.(Default: `10.42.0.1`)

![07](./documents/images/07.png)

Position 을 입력 후 `Done` 을 누른다.(Default 셋팅이 되어있으며 ``1~4` 까지 가능하다.)

1. 왼쪽 상단
2. 오른쪽 상단
3. 왼쪽 하단
4. 오른쪽 하단
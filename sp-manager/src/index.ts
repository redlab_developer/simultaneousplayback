import * as WebSocket from 'ws'
import * as playlist from './playlist.json'
import * as express from 'express'
import { uniqueID } from './util'

type ReqType = {
  position?: number
}

type DeviceWS = {
  id: string
  isReady: boolean
  ws: WebSocket
}

// 영상 파일 index
let playIndex = 0
let timeoutNextPlayLoad: NodeJS.Timeout

// web socket 리스트

const wss = new WebSocket.Server({ port: 8080 })
const app = express()

app.use(express.static('./public'))

app.get('/media-list', (req, res) => {
  const reqQuery: ReqType = req.query
  if (reqQuery.position && reqQuery.position <= 4) {
    let plsJson = playlist
    const result = plsJson.map((media) => {
      return {
        ...media,
        uri: `/media/${reqQuery.position}/${media.fileName}`,
      }
    })
    res.json(result)
  } else {
    res.json([])
  }
})

app.listen(3000, () => {
  console.log('run http://localhost:3000')
})

const deviceWSList: Array<DeviceWS> = []

//접속 이벤트
wss.on('connection', (ws) => {
  const id = uniqueID()

  //id 값을 넣어 deviceList map에 할당
  deviceWSList.push({ id, ws, isReady: false })

  ws.on('message', (data) => {
    const str = data as string

    if (str.indexOf('get_time:') >= 0) {
      const cliTime = parseInt(str.replace('get_time:', ''))
      ws.send(
        `{"type":"time:","stt":${Date.now()},"ctt":${
          isNaN(cliTime) ? -1 : cliTime
        }}`
      )
    } else if (str === 'ready') {
      const index = deviceWSList.findIndex(({ id: wsId }) => wsId === id)
      deviceWSList[index].isReady = true

      const isAllReady =
        deviceWSList.findIndex(({ isReady }) => isReady === false) <= -1
      if (isAllReady) {
        sendMessagePreset('play')
      }
    }
  })

  ws.on('close', () => {
    const index = deviceWSList.findIndex(({ id: wsId }) => wsId === id)
    deviceWSList.splice(index, 1)
    console.log(`close: ${id}`)
  })
  sendMessagePreset('load')
})

/**
 * 다음 play index로 변경한다.
 */
const setNextPlayIndex = () => {
  const nextgPlayIndex = playIndex + 1
  if (nextgPlayIndex >= playlist.length) {
    playIndex = nextgPlayIndex
    return
  }
  playIndex = nextgPlayIndex
}

/**
 * 현재 media
 */
const currentMedia = () => {
  const media = playlist[playIndex]
  if (media === undefined) {
    playIndex = 0
  }
  return playlist[playIndex]
}

/**
 * send message preset
 * @param type
 */
const sendMessagePreset = (type: 'load' | 'play') => {
  if (timeoutNextPlayLoad) {
    clearTimeout(timeoutNextPlayLoad)
  }

  const { duration, fileName } = currentMedia()
  if (type === 'load') {
    const msgReady = `R: ${fileName}`
    deviceWSList.forEach((obj) => {
      obj.isReady = false
    })
    sendAllMessage(msgReady)
    return
  }

  const msgPlay = `P: ${Date.now() + 1000}`
  sendAllMessage(msgPlay)
  deviceWSList.forEach((obj) => {
    obj.isReady = false
  })

  timeoutNextPlayLoad = setTimeout(() => {
    setNextPlayIndex()
    sendMessagePreset('load')
  }, duration)
}

const sendAllMessage = (message: any) => {
  setTimeout(() => {
    deviceWSList.forEach(({ ws }) => ws.send(message))
  }, 10)
}
